
package com.showroomz.mohamedantartask.car_list_module.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FullOption {

    @SerializedName("trim_sortorder")
    @Expose
    private String trimSortorder;
    @SerializedName("trim_price")
    @Expose
    private String trimPrice;
    @SerializedName("trim_spec")
    @Expose
    private List<TrimSpec_> trimSpec = null;

    public String getTrimSortorder() {
        return trimSortorder;
    }

    public void setTrimSortorder(String trimSortorder) {
        this.trimSortorder = trimSortorder;
    }

    public String getTrimPrice() {
        return trimPrice;
    }

    public void setTrimPrice(String trimPrice) {
        this.trimPrice = trimPrice;
    }

    public List<TrimSpec_> getTrimSpec() {
        return trimSpec;
    }

    public void setTrimSpec(List<TrimSpec_> trimSpec) {
        this.trimSpec = trimSpec;
    }

}
