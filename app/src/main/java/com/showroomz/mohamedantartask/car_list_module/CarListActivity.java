package com.showroomz.mohamedantartask.car_list_module;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.showroomz.mohamedantartask.R;
import com.showroomz.mohamedantartask.adapter.CarsListAdapter;
import com.showroomz.mohamedantartask.base.BaseActivity;
import com.showroomz.mohamedantartask.car_details_module.CarDetailsActivity;
import com.showroomz.mohamedantartask.car_list_module.model.Car;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.showroomz.mohamedantartask.utilities.Constant.CAR_ID_KEY;

public class CarListActivity extends BaseActivity {


    /**
     * ButterKnife Code
     **/
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.constraintLayout)
    androidx.constraintlayout.widget.ConstraintLayout constraintLayout;
    @BindView(R.id.pageTitle)
    TextView pageTitle;
    @BindView(R.id.recyclerViewCarsList)
    androidx.recyclerview.widget.RecyclerView recyclerViewCarsList;
    /**
     * ButterKnife Code
     **/
    CarListViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_list);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this).get(CarListViewModel.class);

//        openDetailsScreen("1473");
        init();
    }

    ArrayList<Car> carList;
    CarsListAdapter carsListAdapter;

    private void init() {
        observeToCommonObserver(viewModel);
        carList = new ArrayList<>();
        carsListAdapter = new CarsListAdapter(carList, new CarsListAdapter.onItemClick() {
            @Override
            public void onItemClick(int itemPosition) {
                openDetailsScreen(carList.get(itemPosition).getModelsId());
            }
        });

        recyclerViewCarsList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerViewCarsList.setItemAnimator(new DefaultItemAnimator());
        recyclerViewCarsList.setAdapter(carsListAdapter);
        viewModel.loadAllModelsDate();

        viewModel.getApiAllModelsApiResponseMutableLiveData().observe(this, response -> {
            carList.addAll(response.getData());
            carsListAdapter.notifyDataSetChanged();
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
                viewModel.loadAllModelsDate();
            }
        });
    }


    public void openDetailsScreen(String carId) {
        Intent detailsScreenIntent = new Intent(CarListActivity.this, CarDetailsActivity.class);
        detailsScreenIntent.putExtra(CAR_ID_KEY, carId);
        startActivity(detailsScreenIntent);
    }
}
