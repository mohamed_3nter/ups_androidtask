
package com.showroomz.mohamedantartask.car_list_module.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelsSpecEnUpdated {

    @SerializedName("STD")
    @Expose
    private STD sTD;
    @SerializedName("Full option")
    @Expose
    private FullOption fullOption;

    public STD getSTD() {
        return sTD;
    }

    public void setSTD(STD sTD) {
        this.sTD = sTD;
    }

    public FullOption getFullOption() {
        return fullOption;
    }

    public void setFullOption(FullOption fullOption) {
        this.fullOption = fullOption;
    }

}
