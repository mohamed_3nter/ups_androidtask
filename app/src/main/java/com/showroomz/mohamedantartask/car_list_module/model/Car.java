
package com.showroomz.mohamedantartask.car_list_module.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Car {

    @SerializedName("brands_hotline")
    @Expose
    private String brandsHotline;
    @SerializedName("models_id")
    @Expose
    private String modelsId;
    @SerializedName("models_type")
    @Expose
    private String modelsType;
    @SerializedName("models_information_en")
    @Expose
    private String modelsInformationEn;
    @SerializedName("models_information_ar")
    @Expose
    private String modelsInformationAr;
    @SerializedName("models_brand_id")
    @Expose
    private String modelsBrandId;
    @SerializedName("models_desc_en")
    @Expose
    private String modelsDescEn;
    @SerializedName("models_desc_ar")
    @Expose
    private String modelsDescAr;
    @SerializedName("models_name_en")
    @Expose
    private String modelsNameEn;
    @SerializedName("models_name_ar")
    @Expose
    private String modelsNameAr;
    @SerializedName("models_image")
    @Expose
    private String modelsImage;
    @SerializedName("models_isinitial")
    @Expose
    private String modelsIsinitial;
    @SerializedName("models_year")
    @Expose
    private String modelsYear;
    @SerializedName("models_color")
    @Expose
    private String modelsColor;
    @SerializedName("models_view")
    @Expose
    private String modelsView;
    @SerializedName("models_testdrive_available")
    @Expose
    private String modelsTestdriveAvailable;
    @SerializedName("models_testdrive_atshowroom")
    @Expose
    private String modelsTestdriveAtshowroom;
    @SerializedName("models_testdrive_bydelivery")
    @Expose
    private String modelsTestdriveBydelivery;
    @SerializedName("models_test_drive_price")
    @Expose
    private String modelsTestDrivePrice;
    @SerializedName("models_exterior_image")
    @Expose
    private String modelsExteriorImage;
    @SerializedName("models_interior_available")
    @Expose
    private String modelsInteriorAvailable;
    @SerializedName("models_interior_image")
    @Expose
    private String modelsInteriorImage;
    @SerializedName("models_price")
    @Expose
    private String modelsPrice;
    @SerializedName("models_comingsoon_month")
    @Expose
    private String modelsComingsoonMonth;
    @SerializedName("models_offer")
    @Expose
    private List<Object> modelsOffer = null;
    @SerializedName("models_cashback_offer")
    @Expose
    private List<Object> modelsCashbackOffer = null;
    @SerializedName("models_discount_offer")
    @Expose
    private List<Object> modelsDiscountOffer = null;
    @SerializedName("models_gift_offer")
    @Expose
    private List<Object> modelsGiftOffer = null;
    @SerializedName("models_spec_en")
    @Expose
    private String modelsSpecEn;
    @SerializedName("models_spec_ar")
    @Expose
    private String modelsSpecAr;
    @SerializedName("models_spec_en_updated")
    @Expose
    private ModelsSpecEnUpdated modelsSpecEnUpdated;

    public String getBrandsHotline() {
        return brandsHotline;
    }

    public void setBrandsHotline(String brandsHotline) {
        this.brandsHotline = brandsHotline;
    }

    public String getModelsId() {
        return modelsId;
    }

    public void setModelsId(String modelsId) {
        this.modelsId = modelsId;
    }

    public String getModelsType() {
        return modelsType;
    }

    public void setModelsType(String modelsType) {
        this.modelsType = modelsType;
    }

    public String getModelsInformationEn() {
        return modelsInformationEn;
    }

    public void setModelsInformationEn(String modelsInformationEn) {
        this.modelsInformationEn = modelsInformationEn;
    }

    public String getModelsInformationAr() {
        return modelsInformationAr;
    }

    public void setModelsInformationAr(String modelsInformationAr) {
        this.modelsInformationAr = modelsInformationAr;
    }

    public String getModelsBrandId() {
        return modelsBrandId;
    }

    public void setModelsBrandId(String modelsBrandId) {
        this.modelsBrandId = modelsBrandId;
    }

    public String getModelsDescEn() {
        return modelsDescEn;
    }

    public void setModelsDescEn(String modelsDescEn) {
        this.modelsDescEn = modelsDescEn;
    }

    public String getModelsDescAr() {
        return modelsDescAr;
    }

    public void setModelsDescAr(String modelsDescAr) {
        this.modelsDescAr = modelsDescAr;
    }

    public String getModelsNameEn() {
        return modelsNameEn;
    }

    public void setModelsNameEn(String modelsNameEn) {
        this.modelsNameEn = modelsNameEn;
    }

    public String getModelsNameAr() {
        return modelsNameAr;
    }

    public void setModelsNameAr(String modelsNameAr) {
        this.modelsNameAr = modelsNameAr;
    }

    public String getModelsImage() {
        return modelsImage;
    }

    public void setModelsImage(String modelsImage) {
        this.modelsImage = modelsImage;
    }

    public String getModelsIsinitial() {
        return modelsIsinitial;
    }

    public void setModelsIsinitial(String modelsIsinitial) {
        this.modelsIsinitial = modelsIsinitial;
    }

    public String getModelsYear() {
        return modelsYear;
    }

    public void setModelsYear(String modelsYear) {
        this.modelsYear = modelsYear;
    }

    public String getModelsColor() {
        return modelsColor;
    }

    public void setModelsColor(String modelsColor) {
        this.modelsColor = modelsColor;
    }

    public String getModelsView() {
        return modelsView;
    }

    public void setModelsView(String modelsView) {
        this.modelsView = modelsView;
    }

    public String getModelsTestdriveAvailable() {
        return modelsTestdriveAvailable;
    }

    public void setModelsTestdriveAvailable(String modelsTestdriveAvailable) {
        this.modelsTestdriveAvailable = modelsTestdriveAvailable;
    }

    public String getModelsTestdriveAtshowroom() {
        return modelsTestdriveAtshowroom;
    }

    public void setModelsTestdriveAtshowroom(String modelsTestdriveAtshowroom) {
        this.modelsTestdriveAtshowroom = modelsTestdriveAtshowroom;
    }

    public String getModelsTestdriveBydelivery() {
        return modelsTestdriveBydelivery;
    }

    public void setModelsTestdriveBydelivery(String modelsTestdriveBydelivery) {
        this.modelsTestdriveBydelivery = modelsTestdriveBydelivery;
    }

    public String getModelsTestDrivePrice() {
        return modelsTestDrivePrice;
    }

    public void setModelsTestDrivePrice(String modelsTestDrivePrice) {
        this.modelsTestDrivePrice = modelsTestDrivePrice;
    }

    public String getModelsExteriorImage() {
        return modelsExteriorImage;
    }

    public void setModelsExteriorImage(String modelsExteriorImage) {
        this.modelsExteriorImage = modelsExteriorImage;
    }

    public String getModelsInteriorAvailable() {
        return modelsInteriorAvailable;
    }

    public void setModelsInteriorAvailable(String modelsInteriorAvailable) {
        this.modelsInteriorAvailable = modelsInteriorAvailable;
    }

    public String getModelsInteriorImage() {
        return modelsInteriorImage;
    }

    public void setModelsInteriorImage(String modelsInteriorImage) {
        this.modelsInteriorImage = modelsInteriorImage;
    }

    public String getModelsPrice() {
        return modelsPrice;
    }

    public void setModelsPrice(String modelsPrice) {
        this.modelsPrice = modelsPrice;
    }

    public String getModelsComingsoonMonth() {
        return modelsComingsoonMonth;
    }

    public void setModelsComingsoonMonth(String modelsComingsoonMonth) {
        this.modelsComingsoonMonth = modelsComingsoonMonth;
    }

    public List<Object> getModelsOffer() {
        return modelsOffer;
    }

    public void setModelsOffer(List<Object> modelsOffer) {
        this.modelsOffer = modelsOffer;
    }

    public List<Object> getModelsCashbackOffer() {
        return modelsCashbackOffer;
    }

    public void setModelsCashbackOffer(List<Object> modelsCashbackOffer) {
        this.modelsCashbackOffer = modelsCashbackOffer;
    }

    public List<Object> getModelsDiscountOffer() {
        return modelsDiscountOffer;
    }

    public void setModelsDiscountOffer(List<Object> modelsDiscountOffer) {
        this.modelsDiscountOffer = modelsDiscountOffer;
    }

    public List<Object> getModelsGiftOffer() {
        return modelsGiftOffer;
    }

    public void setModelsGiftOffer(List<Object> modelsGiftOffer) {
        this.modelsGiftOffer = modelsGiftOffer;
    }

    public String getModelsSpecEn() {
        return modelsSpecEn;
    }

    public void setModelsSpecEn(String modelsSpecEn) {
        this.modelsSpecEn = modelsSpecEn;
    }

    public String getModelsSpecAr() {
        return modelsSpecAr;
    }

    public void setModelsSpecAr(String modelsSpecAr) {
        this.modelsSpecAr = modelsSpecAr;
    }

    public ModelsSpecEnUpdated getModelsSpecEnUpdated() {
        return modelsSpecEnUpdated;
    }

    public void setModelsSpecEnUpdated(ModelsSpecEnUpdated modelsSpecEnUpdated) {
        this.modelsSpecEnUpdated = modelsSpecEnUpdated;
    }

}
