package com.showroomz.mohamedantartask.car_list_module;

import androidx.lifecycle.MutableLiveData;

import com.showroomz.mohamedantartask.base.BaseApplication;
import com.showroomz.mohamedantartask.base.BaseViewModel;
import com.showroomz.mohamedantartask.car_list_module.model.AllModelsApiResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarListViewModel extends BaseViewModel {

    protected MutableLiveData<AllModelsApiResponse> apiAllModelsApiResponseMutableLiveData;

    public MutableLiveData<AllModelsApiResponse> getApiAllModelsApiResponseMutableLiveData() {
        if (apiAllModelsApiResponseMutableLiveData == null) {
            apiAllModelsApiResponseMutableLiveData = new MutableLiveData<>();
        }
        return apiAllModelsApiResponseMutableLiveData;
    }

    public void loadAllModelsDate() {
        getProgressDialogShowOrHide().setValue(true);
        BaseApplication.getInstance().getApiClient().getAllModels(new Callback<AllModelsApiResponse>() {
            @Override
            public void onResponse(Call<AllModelsApiResponse> call, Response<AllModelsApiResponse> response) {
                getProgressDialogShowOrHide().setValue(false);
                getApiAllModelsApiResponseMutableLiveData().setValue(response.body());
            }

            @Override
            public void onFailure(Call<AllModelsApiResponse> call, Throwable t) {
                getProgressDialogShowOrHide().setValue(false);
                getApiErrorMessageResponse().setValue(t.getMessage());
            }
        });
    }

}
