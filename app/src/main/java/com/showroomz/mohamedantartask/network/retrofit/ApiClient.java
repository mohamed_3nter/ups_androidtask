package com.showroomz.mohamedantartask.network.retrofit;

import android.content.Context;

import com.google.gson.Gson;
import com.showroomz.mohamedantartask.car_details_module.model.OneModelDetailResponse;
import com.showroomz.mohamedantartask.car_list_module.model.AllModelsApiResponse;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.showroomz.mohamedantartask.utilities.Constant.BASE_URL;


public class ApiClient {

    private static Retrofit retrofitClient = null;
    private static Context context;

    public ApiClient(Context context) {
        this.context = context;
    }

    public static Retrofit getRetrofitClient() {
        if (retrofitClient == null) {

            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(httpLoggingInterceptor)
                    .connectTimeout(50, TimeUnit.SECONDS)
                    .readTimeout(50, TimeUnit.SECONDS)
                    .writeTimeout(50, TimeUnit.SECONDS)
                    .build();

            retrofitClient = new Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
                    .client(httpClient)
                    .baseUrl(BASE_URL)
                    .build();

        }
        return retrofitClient;
    }

    public void getAllModels(Callback<AllModelsApiResponse> callback) {
        ApiInterface apiService =
                ApiClient.getRetrofitClient().create(ApiInterface.class);
        Call<AllModelsApiResponse> call = apiService.getAllModels();
        call.enqueue(callback);
    }

    public void getCarDetails(String carId, Callback<OneModelDetailResponse> callback) {
        ApiInterface apiService =
                ApiClient.getRetrofitClient().create(ApiInterface.class);
        Call<OneModelDetailResponse> call = apiService.getOneModelDetail(carId);
        call.enqueue(callback);
    }


    public void downloadFileWithDynamicUrlSync(String url, Callback<ResponseBody> callback) {
        ApiInterface apiService =
                ApiClient.getRetrofitClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.downloadFileWithDynamicUrlSync(url);
        call.enqueue(callback);
    }

}
