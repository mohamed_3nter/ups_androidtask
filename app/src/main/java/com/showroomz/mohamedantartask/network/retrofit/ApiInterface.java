package com.showroomz.mohamedantartask.network.retrofit;


import com.showroomz.mohamedantartask.car_details_module.model.OneModelDetailResponse;
import com.showroomz.mohamedantartask.car_list_module.model.AllModelsApiResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiInterface {


    @GET("/dev/api/api.php?function=allmodels")
    Call<AllModelsApiResponse> getAllModels();


    @GET("/api/api.php?function=allmodelsbybrand")
    Call<OneModelDetailResponse> getOneModelDetail(@Query("carId") String carId);


    @GET
    Call<ResponseBody> downloadFileWithDynamicUrlSync(@Url String fileUrl);


}
