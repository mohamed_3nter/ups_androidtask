
package com.showroomz.mohamedantartask.car_details_module.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TrimSpec {

    @SerializedName("DRIVETRAIN")
    @Expose
    private List<String> dRIVETRAIN = null;
    @SerializedName("EXTERIOR FEATURES")
    @Expose
    private List<String> eXTERIORFEATURES = null;
    @SerializedName("INTERIOR FEATURES")
    @Expose
    private List<String> iNTERIORFEATURES = null;
    @SerializedName("SAFETY")
    @Expose
    private List<String> sAFETY = null;

    public List<String> getDRIVETRAIN() {
        return dRIVETRAIN;
    }

    public void setDRIVETRAIN(List<String> dRIVETRAIN) {
        this.dRIVETRAIN = dRIVETRAIN;
    }

    public List<String> getEXTERIORFEATURES() {
        return eXTERIORFEATURES;
    }

    public void setEXTERIORFEATURES(List<String> eXTERIORFEATURES) {
        this.eXTERIORFEATURES = eXTERIORFEATURES;
    }

    public List<String> getINTERIORFEATURES() {
        return iNTERIORFEATURES;
    }

    public void setINTERIORFEATURES(List<String> iNTERIORFEATURES) {
        this.iNTERIORFEATURES = iNTERIORFEATURES;
    }

    public List<String> getSAFETY() {
        return sAFETY;
    }

    public void setSAFETY(List<String> sAFETY) {
        this.sAFETY = sAFETY;
    }

}
