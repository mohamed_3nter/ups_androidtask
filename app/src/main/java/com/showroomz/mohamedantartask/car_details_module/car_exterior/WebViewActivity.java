package com.showroomz.mohamedantartask.car_details_module.car_exterior;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.showroomz.mohamedantartask.R;
import com.showroomz.mohamedantartask.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.showroomz.mohamedantartask.utilities.Constant.CAR_EXTERIOR_KEY;
import static com.showroomz.mohamedantartask.utilities.ProgressDialogCustom.dismissProgressDialog;
import static com.showroomz.mohamedantartask.utilities.ProgressDialogCustom.showProgressDialog;

public class WebViewActivity extends BaseActivity {


    @BindView(R.id.webViewExternal)
    WebView webViewExternal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        ButterKnife.bind(this);
        initWebView();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initWebView() {
        String url = getIntent().getStringExtra(CAR_EXTERIOR_KEY);
        webViewExternal.clearCache(true);
        webViewExternal.getSettings().setJavaScriptEnabled(true);
        webViewExternal.clearHistory();
        webViewExternal.getSettings().setDisplayZoomControls(false);
        webViewExternal.getSettings().setUseWideViewPort(true);
        webViewExternal.requestFocus(View.FOCUS_DOWN);
        webViewExternal.getSettings().setLoadWithOverviewMode(true);
        webViewExternal.getSettings().setBuiltInZoomControls(false);
        webViewExternal.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                dismissProgressDialog();

            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                dismissProgressDialog();


            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showProgressDialog(WebViewActivity.this);

            }
        });
        webViewExternal.loadUrl(url);

    }

    public void closeScreen(View view) {
        this.finish();
    }
}
