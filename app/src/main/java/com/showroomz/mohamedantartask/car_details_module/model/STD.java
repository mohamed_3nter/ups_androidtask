
package com.showroomz.mohamedantartask.car_details_module.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class STD {

    @SerializedName("trim_sortorder")
    @Expose
    private String trimSortorder;
    @SerializedName("trim_price")
    @Expose
    private String trimPrice;
    @SerializedName("trim_spec")
    @Expose
    private List<TrimSpec> trimSpec = null;

    public String getTrimSortorder() {
        return trimSortorder;
    }

    public void setTrimSortorder(String trimSortorder) {
        this.trimSortorder = trimSortorder;
    }

    public String getTrimPrice() {
        return trimPrice;
    }

    public void setTrimPrice(String trimPrice) {
        this.trimPrice = trimPrice;
    }

    public List<TrimSpec> getTrimSpec() {
        return trimSpec;
    }

    public void setTrimSpec(List<TrimSpec> trimSpec) {
        this.trimSpec = trimSpec;
    }

}
