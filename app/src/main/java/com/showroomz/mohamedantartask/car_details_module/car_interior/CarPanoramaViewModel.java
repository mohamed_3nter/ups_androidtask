package com.showroomz.mohamedantartask.car_details_module.car_interior;

import androidx.lifecycle.MutableLiveData;

import com.showroomz.mohamedantartask.base.BaseApplication;
import com.showroomz.mohamedantartask.base.BaseViewModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarPanoramaViewModel extends BaseViewModel {

    protected MutableLiveData<ResponseBody> apiLoadFileByURLiveData;

    public MutableLiveData<ResponseBody> getApiLoadFileByURLiveData() {
        if (apiLoadFileByURLiveData == null) {
            apiLoadFileByURLiveData = new MutableLiveData<>();
        }
        return apiLoadFileByURLiveData;
    }

    public void loadFileByURL(String url) {
        getProgressDialogShowOrHide().setValue(true);
        BaseApplication.getInstance().getApiClient().downloadFileWithDynamicUrlSync(url, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                getProgressDialogShowOrHide().setValue(false);
                getApiLoadFileByURLiveData().setValue(response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                getProgressDialogShowOrHide().setValue(false);
                getApiErrorMessageResponse().setValue(t.getMessage());
            }
        });
    }


}
