package com.showroomz.mohamedantartask.car_details_module;

import androidx.lifecycle.MutableLiveData;

import com.showroomz.mohamedantartask.base.BaseApplication;
import com.showroomz.mohamedantartask.base.BaseViewModel;
import com.showroomz.mohamedantartask.car_details_module.model.OneModelDetailResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarDetailsViewModel extends BaseViewModel {

    protected MutableLiveData<OneModelDetailResponse> apiOneModelDetailResponseMutableLiveData;

    public MutableLiveData<OneModelDetailResponse> getApiOneModelDetailResponseMutableLiveData() {
        if (apiOneModelDetailResponseMutableLiveData == null) {
            apiOneModelDetailResponseMutableLiveData = new MutableLiveData<>();
        }
        return apiOneModelDetailResponseMutableLiveData;
    }

    public void loadCarDetailsDate(String carId) {
        getProgressDialogShowOrHide().setValue(true);
        BaseApplication.getInstance().getApiClient().getCarDetails(carId, new Callback<OneModelDetailResponse>() {
            @Override
            public void onResponse(Call<OneModelDetailResponse> call, Response<OneModelDetailResponse> response) {
                getProgressDialogShowOrHide().setValue(false);
                getApiOneModelDetailResponseMutableLiveData().setValue(response.body());
            }

            @Override
            public void onFailure(Call<OneModelDetailResponse> call, Throwable t) {
                getProgressDialogShowOrHide().setValue(false);
                getApiErrorMessageResponse().setValue(t.getMessage());
            }
        });
    }

}
