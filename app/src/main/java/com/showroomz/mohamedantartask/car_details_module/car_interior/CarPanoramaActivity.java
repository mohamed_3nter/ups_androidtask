package com.showroomz.mohamedantartask.car_details_module.car_interior;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import androidx.lifecycle.ViewModelProviders;

import com.panoramagl.PLICamera;
import com.panoramagl.PLImage;
import com.panoramagl.PLManager;
import com.panoramagl.PLSphericalPanorama;
import com.showroomz.mohamedantartask.R;
import com.showroomz.mohamedantartask.base.BaseActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import okhttp3.ResponseBody;

import static com.showroomz.mohamedantartask.utilities.Constant.CAR_INTERIOR_KEY;

public class
CarPanoramaActivity extends BaseActivity {


    private PLManager plManager;
    CarPanoramaViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_panorama);
        viewModel = ViewModelProviders.of(this).get(CarPanoramaViewModel.class);
        plManager = new PLManager(this);
        observeToCommonObserver(viewModel);
        init();

    }

    private void init() {

        String url = getIntent().getStringExtra(CAR_INTERIOR_KEY);
        viewModel.getApiLoadFileByURLiveData().observe(this, responseBody -> {
            boolean FileDownloaded = DownloadImage(responseBody);
        });
        viewModel.loadFileByURL(url);

    }

    private void initPlView(Bitmap bitmap) {

        ImageView loadedImage = findViewById(R.id.imageViewLoadedImage);
        loadedImage.setImageBitmap(bitmap);
        plManager.setContentView(findViewById(R.id.content_view));
        plManager.onCreate();

        plManager.setAccelerometerEnabled(false);
        plManager.setInertiaEnabled(false);
        plManager.setZoomEnabled(false);


        PLSphericalPanorama panorama = new PLSphericalPanorama();

        //the panorama view work only with local file,
        //panorama.setImage(new PLImage(PLUtils.getBitmap(this, R.raw.sighisoara_sphere), false));

        panorama.setImage(new PLImage(bitmap, true));
        float pitch = 5f;
        float yaw = 0f;
        float zoomFactor = 0.8f;

        PLICamera camera = plManager.getPanorama().getCamera();
        pitch = camera.getPitch();
        yaw = camera.getYaw();
        zoomFactor = camera.getZoomFactor();

        panorama.getCamera().lookAtAndZoomFactor(pitch, yaw, zoomFactor, false);
        plManager.setPanorama(panorama);

    }

    @Override
    protected void onResume() {
        super.onResume();
        plManager.onResume();
    }

    @Override
    protected void onPause() {
        plManager.onPause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        plManager.onDestroy();
        super.onDestroy();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return plManager.onTouchEvent(event);
    }

    public void closeScreen(View view) {
        this.finish();
    }


    private boolean DownloadImage(ResponseBody body) {

        try {
            InputStream in = null;
            FileOutputStream out = null;

            try {
                in = body.byteStream();
                out = new FileOutputStream(getExternalFilesDir(null) + File.separator + "Interior.jpg");
                int c;

                while ((c = in.read()) != -1) {
                    out.write(c);
                }
            } catch (IOException e) {
                return false;
            } finally {

                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            }


            Bitmap bMap = BitmapFactory.decodeFile(getExternalFilesDir(null) + File.separator + "Interior.jpg");
            InputStream stream = new FileInputStream(getExternalFilesDir(null) + File.separator + "Interior.jpg");
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inDither = true;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap bitmap = BitmapFactory.decodeStream(stream, new Rect(), options);
            initPlView(bitmap);
            try {
                stream.close();
            } catch (IOException e) {
                // do nothing here
            }

            return true;

        } catch (IOException e) {
            Log.d("DownloadImage", e.toString());
            return false;
        }
    }


}