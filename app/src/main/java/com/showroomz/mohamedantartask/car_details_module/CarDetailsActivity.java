package com.showroomz.mohamedantartask.car_details_module;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.showroomz.mohamedantartask.R;
import com.showroomz.mohamedantartask.base.BaseActivity;
import com.showroomz.mohamedantartask.car_details_module.model.Car;
import com.showroomz.mohamedantartask.car_details_module.car_exterior.WebViewActivity;
import com.showroomz.mohamedantartask.car_details_module.car_interior.CarPanoramaActivity;
import com.showroomz.mohamedantartask.utilities.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.showroomz.mohamedantartask.utilities.Constant.CAR_ID_KEY;

public class CarDetailsActivity extends BaseActivity {


    @BindView(R.id.imageViewCarImage)
    ImageView imageViewCarImage;

    @BindView(R.id.textViewItemTitle)
    TextView textViewItemTitle;
    @BindView(R.id.textViewNumOfSeen)
    TextView textViewNumOfSeen;
    @BindView(R.id.textViewPrice)
    TextView textViewPrice;
    @BindView(R.id.textViewModel)
    TextView textViewModel;
    @BindView(R.id.textViewItemDescription)
    TextView textViewItemDescription;
    @BindView(R.id.textViewColorsHint)
    TextView textViewColorsHint;
    @BindView(R.id.linearLayoutColors)
    LinearLayout linearLayoutColors;


    String carId;
    CarDetailsViewModel viewModel;

    Car carData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_details);
        ButterKnife.bind(this);
        viewModel = ViewModelProviders.of(this).get(CarDetailsViewModel.class);
        carId = getIntent().getStringExtra(CAR_ID_KEY);
        init();
    }

    private void init() {
        observeToCommonObserver(viewModel);
        viewModel.loadCarDetailsDate(carId);
        viewModel.getApiOneModelDetailResponseMutableLiveData().observe(this, oneModelDetailResponse -> {
            carData = oneModelDetailResponse.getData().get(0);
            bindData();
        });
    }

    private void bindData() {
        addAvailableColor(carData.getModelsColor());
        Glide.with(imageViewCarImage.getContext()).load(Constant.BASE_URL + "api/" + carData.getModelsImage())
                .placeholder(R.drawable.place_holder_car) //placeholder
                .error(R.drawable.place_holder_car) //error
                .into(imageViewCarImage);

        textViewItemTitle.setText(carData.getModelsNameEn());
        textViewItemDescription.setText(carData.getModelsInformationEn());
        textViewModel.setText(carData.getModelsYear());
        textViewPrice.setText(getString(R.string.str_price_amount, carData.getModelsPrice()));
        textViewNumOfSeen.setText(carData.getModelsView());
    }

    public void openInterior(View view) {
        Intent i = new Intent(CarDetailsActivity.this, CarPanoramaActivity.class);
        i.putExtra(Constant.CAR_INTERIOR_KEY, "https://showroomz.com/" + carData.getModelsInteriorImage());
        startActivity(i);

    }

    public void openExterior(View view) {
        Intent i = new Intent(CarDetailsActivity.this, WebViewActivity.class);
        i.putExtra(Constant.CAR_EXTERIOR_KEY, carData.getModelsExteriorImage());
        startActivity(i);
    }


    public void onBackPress(View view) {
        onBackPressed();
    }

    private void addAvailableColor(String colors) {
        String[] availableColor = colors.split(",");
        for (int i = 0; i < availableColor.length; i++) {
            View colorItem = getLayoutInflater().inflate(R.layout.color_item, null);
            View colorView = colorItem.findViewById(R.id.colorView);
            colorView.setBackgroundColor(Color.parseColor(availableColor[i].trim()));
            linearLayoutColors.addView(colorItem);
        }

    }
}
