package com.showroomz.mohamedantartask.base;

import android.app.Application;
import android.content.Context;

import com.showroomz.mohamedantartask.network.retrofit.ApiClient;

public class BaseApplication extends Application {

    private static BaseApplication mInstance;
    private Context context;
    private ApiClient apiClient;


    public static synchronized BaseApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        this.context = getApplicationContext();
        apiClient = new ApiClient(this);

    }


    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public ApiClient getApiClient() {
        return apiClient;
    }
}
