package com.showroomz.mohamedantartask.base;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BaseViewModel extends ViewModel {

    protected MutableLiveData<String> apiErrorMessageResponse;
    private MutableLiveData<Boolean> progressDialogShowOrHide;

    public MutableLiveData<String> getApiErrorMessageResponse() {
        if (apiErrorMessageResponse == null) {
            apiErrorMessageResponse = new MutableLiveData<>();
        }
        return apiErrorMessageResponse;
    }

    public MutableLiveData<Boolean> getProgressDialogShowOrHide() {
        if (progressDialogShowOrHide == null) {
            progressDialogShowOrHide = new MutableLiveData<>();
        }
        return progressDialogShowOrHide;
    }

}
