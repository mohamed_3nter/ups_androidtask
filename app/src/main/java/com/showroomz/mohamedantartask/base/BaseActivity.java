package com.showroomz.mohamedantartask.base;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import static com.showroomz.mohamedantartask.utilities.ProgressDialogCustom.dismissProgressDialog;
import static com.showroomz.mohamedantartask.utilities.ProgressDialogCustom.showProgressDialog;

/**
 * Created By Mohamed Antar on 13/1/2020.
 */
public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void observeToCommonObserver(BaseViewModel viewModel) {

        viewModel.getApiErrorMessageResponse().observe(this, t -> {
            Toast.makeText(BaseActivity.this,t,Toast.LENGTH_LONG).show();
        });

        viewModel.getProgressDialogShowOrHide().observe(this, t -> {
            if (t)
                showProgressDialog(this);
            else
                dismissProgressDialog();
        });
    }
}
