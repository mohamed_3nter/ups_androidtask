package com.showroomz.mohamedantartask.utilities;

public class Constant {
    public static final String BASE_URL = "https://showroomz.com/";
    public static final String CAR_INTERIOR_KEY = "CAR_INTERIOR";
    public static final String CAR_ID_KEY = "CAR_ID";
    public static final String CAR_EXTERIOR_KEY = "CAR_EXTERIOR";
}
