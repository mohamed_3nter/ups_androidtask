package com.showroomz.mohamedantartask.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.showroomz.mohamedantartask.R;
import com.showroomz.mohamedantartask.car_list_module.model.Car;
import com.showroomz.mohamedantartask.utilities.Constant;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarsListAdapter extends RecyclerView.Adapter<CarsListAdapter.MyViewHolder> {

    private List<Car> items;
    private onItemClick itemClick;

    public CarsListAdapter(List<Car> items, onItemClick itemClick) {
        this.items = items;
        this.itemClick = itemClick;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.car_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Car item = items.get(position);
        Glide.with(holder.imageViewCarImage.getContext()).load(Constant.BASE_URL + "api/" + item.getModelsImage())
                .placeholder(R.drawable.place_holder_car) //placeholder
                .error(R.drawable.place_holder_car) //error
                .into(holder.imageViewCarImage);

        holder.textViewItemTitle.setText(item.getModelsNameEn());
        holder.textViewItemDescription.setText(item.getModelsInformationEn());
        holder.textViewModelYear.setText(holder.textViewModelYear.getContext().getString(R.string.str_model, item.getModelsYear()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClick.onItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        /**
         * ButterKnife Code
         **/
        @BindView(R.id.itemView)
        androidx.constraintlayout.widget.ConstraintLayout itemView;
        @BindView(R.id.imageViewCarImage)
        ImageView imageViewCarImage;
        @BindView(R.id.textViewItemTitle)
        TextView textViewItemTitle;
        @BindView(R.id.textViewItemDescription)
        TextView textViewItemDescription;
        @BindView(R.id.textViewModelYear)
        TextView textViewModelYear;

        /**
         * ButterKnife Code
         **/

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

    }

    public interface onItemClick {

        void onItemClick(int itemPosition);
    }
}